from django.shortcuts import render, redirect
from . models import  Book, Category, Profile
from . models import  Book, Category, Profile
from .forms import *
from django.contrib import messages
from django.contrib.auth import authenticate, logout, login


def index(request):
    return render(request, "index.html")

def student_home(request):
    return render(request, "student_home.html")
    
def book_list(request):
    books = Book.objects.all()
    return render(request, "book_list.html", {'book_list': books})

def add_book(request):
    if request.method == 'POST':
        forms = AddBookForm(request.POST)
        if forms.is_valid():
            forms.save()
            messages.success(request, 'Book added successfully')
            return redirect('add-book')
    else:
        forms = AddBookForm()
        context = {'forms':forms}
        return render(request, 'add_book.html', context)


def edit_book(request,pk):
    item = Book.objects.get(id=pk)
    if request.method == 'POST':
        forms = AddBookForm(request.POST,instance=item)
        if forms.is_valid():
            forms.save()
            messages.success(request, 'Book edited successfully')
            return redirect('books')
    else:
        forms = AddBookForm(instance=item)
        context = {'forms':forms}
        return render(request, 'edit_book.html', context)


def issue_book(request):
    if request.method == 'POST':
        forms = IssueBookForm(request.POST)
        if forms.is_valid():
            forms.save()
            messages.success(request, 'Book issued successfully')
            return redirect('issue-book')
    else:
        forms = IssueBookForm()
        context = {'forms':forms}
        return render(request, 'issue_book.html', context)

def add_category(request):
    if request.method == 'POST':
        forms = AddCategoryForm(request.POST)
        if forms.is_valid():
            forms.save()
            messages.success(request, 'Category added successfully')
            return redirect('add-category')
    else:
        forms = AddCategoryForm()
        context = {'forms':forms}
        return render(request, 'add_category.html', context)


def category_list(request):
    category = Category.objects.all()
    context = {'category_list':category}
    return render(request, "category_list.html", context )


def edit_category(request,pk):
    item = Category.objects.get(id=pk)
    if request.method == 'POST':
        forms = AddCategoryForm(request.POST,instance=item)
        if forms.is_valid():
            forms.save()
            messages.success(request, 'Category edited successfully')
            return redirect('category')
    else:
        forms = AddCategoryForm(instance=item)
        context = {'forms':forms}
        return render(request, 'edit_category.html', context)

def student_signup(request):
    if request.method == 'POST':
        forms = StudentSignupForm(request.POST,request.FILES)
       
        if forms.is_valid():
            forms.save()
            messages.success(request, 'ACCOUNT SUCCESSFULLY CREATED')
            
            return redirect('student-home')
    else:
        forms = StudentSignupForm()
        context = {'forms':forms}
        return render(request, 'studentsignup.html', context)

def student_signin(request):
    logout(request)
    if request.method == 'POST':
        forms = StudentSigninForm(request.POST)
         
        if forms.is_valid():
            userName = forms.cleaned_data['username']
            passWord = forms.cleaned_data['password']
            print(forms)
            user = authenticate(username=userName, password=passWord)
             
            if user is not None:
                if user.is_active:
                    login(request,user)
                    if user.is_staff:
                        return redirect('/')
                    else:
                        return redirect('student-home')
                else:
                    return render(request, 'studentsignin.html', {'forms':forms})
            else:
               return render(request, 'studentsignin.html', {'forms':forms}) 
    forms=StudentSigninForm()
    return render(request, 'studentsignin.html', {'forms':forms})


def search(request):
    if request.method == 'POST':
        book_name = request.POST.get('search')
        status = Book.objects.filter(title__icontains= book_name)
        context = {'books':status}
        return render(request,'search.html', context)
    else:
        return render(request,'search.html')




          