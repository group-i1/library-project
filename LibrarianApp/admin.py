from django.contrib import admin
from .models import Book, Borrow, Payment, Category, Profile

admin.site.register(Book)
admin.site.register(Borrow)
admin.site.register(Payment)
admin.site.register(Category)
admin.site.register(Profile)



