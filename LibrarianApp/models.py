from django.db import models
from django.contrib.auth.models import AbstractUser


class Category(models.Model):
    name = models.CharField(max_length=200,)
    description = models.TextField()
    def __str__(self):
        return self.name


class Book(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE,related_name="category_id_fk")
    title = models.CharField(max_length=200,)
    author = models.CharField(max_length=200,)
    book_num = models.FloatField()
    
    def __str__(self):
        return self.title


class Borrow(models.Model):
    student = models.CharField(max_length=200,default="1", blank=True, null=True )
    book = models.ForeignKey(Book, on_delete=models.CASCADE,related_name="book_id_fk")
    borrowing_date = models.DateField()
    return_date = models.DateField()
    status = models.CharField(max_length=2,choices=(("1","Pending"),("2","Returned")), default=1)

    def __str__(self):
        return self.book.title


class Payment(models.Model):
    amount = models.FloatField()

    def __str__(self):
        return self.amount

class Profile(AbstractUser):
    gender = models.CharField(max_length=2,choices=(("1","Male"),("2","Female")), default=1)
    avatar = models.FileField(upload_to="media/avatars", null=True, blank=True)
    student_num = models.IntegerField(null=True, blank=True)
    course = models.CharField(max_length=200,null=True, blank=True)
    phone_num = models.CharField(max_length=200,null=True, blank=True)

    def __str__(self):
        return self.first_name






    