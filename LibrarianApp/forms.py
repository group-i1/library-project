from django import forms
from .models import *

class AddCategoryForm(forms.ModelForm):
    class Meta:
        model = Category 
        fields = '__all__'
        
class AddBookForm(forms.ModelForm):
    class Meta:
        model = Book 
        fields = '__all__'

        
class IssueBookForm(forms.ModelForm):
    class Meta:
        model = Borrow 
        fields = '__all__'

class StudentSignupForm(forms.ModelForm):
    class Meta:
        model = Profile 
        fields = ('avatar','first_name','last_name','email','student_num','phone_num','course','gender','username','password')

class StudentSigninForm(forms.Form):
    username = forms.CharField(label='Username')
    password = forms.CharField(label='Password')

        