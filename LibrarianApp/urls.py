from django.urls import path
from . import views



urlpatterns=[
    path('', views.index, name='home'),
    path('student-home', views.student_home, name='student-home'),
    path('book', views.book_list, name='books'),
    path('category', views.category_list, name='category'),
    path('add-category', views.add_category, name='add-category'),
    path('add-book', views.add_book, name='add-book'),
    path('issue-book', views.issue_book, name='issue-book'),
    path('category/<int:pk>/edit', views.edit_category, name='edit-category'),
    path('book/<int:pk>/edit', views.edit_book, name='edit-book'), 
    path('student-signup', views.student_signup, name='sign-up'),
    path('student-signin', views.student_signin, name='sign-in'),
    path('search', views.search, name='search'),




    
    
]